<?php
namespace MyAcl;

interface ACEInterface
{

    public function setRole(RoleInterface $role);
    public function setResource(ResourceInterface $resource);
    public function setPermissions($permissions);
    public function setOptions(array $options);
    public function getRole();
    public function getResource();
    public function getPermissions();
    public function getOptions();
    public function isAllowed(array $params = array());

}
