<?php
namespace MyAcl;

interface ACLInterface
{
    public function addPermission(PermissionInterface $permission);
    public function removePermission(PermissionInterface $permission);
    public function hasPermission(PermissionInterface $permission);
    public function addRole(RoleInterface $role, array $parentRoles=array());
    public function removeRole(RoleInterface $role);
    public function hasRole(RoleInterface $role);
    public function addRoleParent(RoleInterface $role, RoleInterface $parentRole);
    public function removeRoleParent(RoleInterface $role, RoleInterface $parentRole);
    public function removeRoleParents(RoleInterface $role);
    public function isRoleParent(RoleInterface $role, RoleInterface $parentRole);
    public function getRoleParents(RoleInterface $role);
    public function getRoleChildren(RoleInterface $parentRole);
    public function getRolesByClass($className);
    public function addResource(ResourceInterface $resource, array $parentResources = array());
    public function removeResource(ResourceInterface $resource);
    public function hasResource(ResourceInterface $resource);
    public function addResourceParent(ResourceInterface $resource, ResourceInterface $parentResource);
    public function removeResourceParent(ResourceInterface $resource, ResourceInterface $parentResource);
    public function removeResourceParents(ResourceInterface $resource);
    public function isResourceParent(ResourceInterface $resource, ResourceInterface $parentResource);
    public function getResourceParents(ResourceInterface $resource);
    public function getResourceChildren(ResourceInterface $parentResource);
    public function getResourcesByClass($className);
    public function addACE(ACEInterface $ace);
    public function removeACE(ACEInterface $ace);
    public function hasACE(ACEInterface $ace);
    public function isAllowed(RoleInterface $role, ResourceInterface $resource, PermissionInterface $permission, array $params = array());

}
