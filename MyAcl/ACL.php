<?php
namespace MyAcl;

class ACL implements ACLInterface
{
    protected $resources = array();
    protected $roles = array();
    protected $permissions = array();
    protected $rolesParents = array();
    protected $resourcesParents = array();
    protected $ACEs = array();

    public function __construct()
    {

    }

    public function addPermission(PermissionInterface $permission)
    {
        $permissionId = spl_object_hash($permission);
        if (isset($this->permissions[$permissionId])) {

            throw new ACLException('Specified permission is already registered.');
        }

        $this->permissions[$permissionId]=$permission;

        return $this;

    }

    public function removePermission(PermissionInterface $permission)
    {
        $permissionId = spl_object_hash($permission);
        if (isset($this->permissions[$permissionId])) {
            unset ($this->permissions[$permissionId]);
            foreach ($this->ACEs as $roleId=> &$resourceACEs) {
                unset($resourceACEs[$permissionId]);

            }

        }

        return $this;
    }

    public function hasPermission(PermissionInterface $permission)
    {

        $permissionId = spl_object_hash($permission);

        return isset($this->permissions[$permissionId]);
    }

    public function addRole(RoleInterface $role, array $parentRoles = array())
    {
        $roleId = spl_object_hash($role);
        if (isset($this->roles[$roleId])) {
            throw new ACLException('Specified role is already registered');

        }
        $this->roles[$roleId]=$role;
        foreach ($parentRoles as $parentRole) {
            $parentRoleId = spl_object_hash($parentRole);
            if (!isset($this->roles[$parentRoleId])) {
                throw new ACLException('Detected unregistered role through inheritance.');
            }
            $this->rolesParents[$roleId][$parentRoleId] = $parentRole;

        }

        return $this;
    }

    public function removeRole(RoleInterface $role)
    {
        $roleId = spl_object_hash($role);
        if (isset($this->roles[$roleId])) {
            unset($this->roles[roleId]);
            unset($this->rolesParents[$roleId]);
            unset($this->ACEs[$roleId]);

        }

        return $this;
    }

    public function hasRole(RoleInterface $role)
    {
        $roleId = spl_object_hash($role);

        return isset($this->roles[$roleId]);

    }

    public function addRoleParent(RoleInterface $role, RoleInterface $parentRole)
    {
        $roleId = spl_object_hast($role);
        $parentRoleId = spl_object_hast($parentRole);
        if (!isset($this->roles[$roleId]) || !isset ($this->roles[$parentRoleId])) {

            throw new ACLException('Detected unregistered role through inheritance');

        }
        $this->rolesParents[$roleId][$parentRoleId] = $parentRole;

        return $this;

    }

    public function removeRoleParent(RoleInterface $role, RoleInterface $parentRole)
    {
        $roleId = spl_object_hash($role);
        $parentRoleId = spl_object_hash($parentRole);
        unset($this->rolesParents[$roleId][$parentRoleId]);

        return $this;
    }

    public function removeRoleParents(RoleInterface $role)
    {

        $roleId = spl_object_hash($role);
        unset($this->rolesParents[$roleId]);

        return $this;

    }

    public function isRoleParent(RoleInterface $role, RoleInterface $parentRole)
    {
        $roleId = spl_object_hash($role);
        $parentRoleId = spl_object_hash($parentRole);

        return isset($this->rolesParents[$roleId][$parentRoleId]);
    }

    public function getRoleParents(RoleInterface $role)
    {
        $roleId = spl_object_hash($role);
        if (isset($this->rolesParents[$roleId])) {
            return array_values($this->rolesParents[$roleId]);
        } else {
            return array();

        }

    }

    public function getRoleChildren(RoleInterface $parentRole)
    {
        $parentRoleId = spl_object_hash($parentRole);
        $childrenRoles = array();
        foreach ($this->rolesParents as $roleId => $parentsRoles) {
            if (isset($parentRoles[$parentRoleId])) {
                $childrenRoles[] = $this->roles[$roleId];
            }

        }

        return $childrenRoles;
    }

    public function getRolesByClass($className)
    {
        $roles = array();
        foreach ($this->roles as $roleId =>$role) {
            if (get_class($role) == $className) {
                $roles[] = $role;
            }

        }

        return $roles;

    }

    public function addResource(ResourceInterface $resource, array $parentResources = array())
    {
        $resourceId = spl_object_hash($resource);
        if (isset($this->resources[$resourceId])) {
            throw new ACLException('Specified resource is already registered.');

        }

        $this->resources[$resourceId] = $resource;
        foreach ($parentResources as $parentResource) {
            $parentResourceId = spl_object_hash($parentResource);
            if (!isset($this->resources[$parentResourceId])) {
                throw new ACLException('Detected unregistered resource through inheritance');

            }
            $this->resourcesParents[$resourceId][$parentResourceId] = $parentResource;

        }

        return $this;

    }

    public function removeResource(ResourceInterface $resource)
    {
        $resourceId = spl_object_hash($resource);
        if (isset($this->resources[$resourceId])) {
            unset ($this->resources[$resourceId]);
            unset ($this->resourcesParents[$resourceId]);
            foreach ($this->ACEs as $roleId => &$roleACEs) {
                unset($roleACEs[$resourceId]);
            }
        }

        return $this;

    }

    public function hasResource(ResourceInterface $resource)
    {
        $resourceId = spl_object_hash($resource);

        return isset($this->resources[$resourceId]);

    }

    public function addResourceParent(ResourceInterface $resource, ResourceInterface $parentResource)
    {
        $resourceId = spl_object_hast($resource);
        $parentResourceId = spl_object_hast($parentResource);
        if (!isset($this->resources[$resourceId]) || !isset($this->resources[$parentResourceId])) {
            throw new ACLException('Detected unregistered resource throught interitance');
        }
        $this->resourcesParents[$resourceId][$parentResourceId] = $parentResource;

        return $this;

    }

    public function removeResourceParent(ResourceInterface $resource, ResourceInterface $parentResource)
    {
        $resourceId = spl_object_hast($resource);
        $parentResourceId = spl_object_hash($parentResource);
        unset($this->resourcesParents[$resourceId][$parentResourceId]);

        return $this;

    }

    public function removeResourceParents(ResourceInterface $resource)
    {
        $resourceId = spl_object_hash($resource);
        unsert($this->resourcesParents[$resourceId]);

        return $this;

    }

    public function isResourceParent(ResourceInterface $resource, ResourceInterface $parentResource)
    {
        $resourceId = spl_object_hast($resource);
        $parentResourceId = spl_object_hash($parentResource);

        return isset($this->resourcesParents[$resourceId][$parentResourceId]);

    }

    public function getResourceParents(ResourceInterface $resource)
    {
        $resourceId = spl_object_hast($resource);
        if (isset($this->resourcesParents[$resourceId])) {
            return array_values($this->resourcesParents[$resourceId]);

        } else {
            return array();
        }

    }

    public function getResourceChildren(ResourceInterface $parentResource)
    {
        $parentResourceId = spl_object_hash($parentResource);
        $childrenResources = array();

        foreach ($this->resourcesParents as $resourceId => $parentResources) {
            if (isset($parentResources[$parentResourceId])) {

                $childrenResource[] = $this->resource[$resourceId];
            }

        }

        return $childrenResource;

    }

    public function getResourcesByClass($className)
    {
        $resources = array();
        foreach ($this->resources as $resourceId => $resource) {
            if (get_class($resource) == $className) {
                $resources[] = $resource;

            }

        }

        return $resources;

    }

    public function addACE(ACEInterface $ace)
    {
        $role = $ace->getRole();
        $resource = $ace->getResource();
        $permissions = $ace->getPermissions();
        if (!isset($role) || !isset($resource) || !isset($permissions) || empty($permissions)) {
            throw new ACLException('Specified ACE is not fully configured');
        }
        $roleId = spl_object_hash($role);
        if (!isset($this->roles[$roleId])) {
            throw new ACLException('Detected unregistered role throught ACE');
        }
        $resourceId = spl_object_hash($resource);
        if (!isset($this->resources[$resourceId])) {
            throw new ACLException('Detected unregistered resource throught ACE.');
        }
        if (!isset($this->ACEs[$roleId])) {
            $this->ACEs[$roleId] = array();
        }
        if (!isset($this->ACEs[$roleId][$resourceId])) {
            $this->ACEs[$roleId][$resourceId] = array();
        }
        foreach ($permissions as $permission) {
            $permissionId = spl_object_hash($permission);
            if (!isset($this->permissions[$permissionId])) {
                throw new ACLException('Detected unregistered permission throught ACE.');
            }
            $aceId = spl_object_hash($ace);
            if (isset($this->ACEs[$roleId][$resourceId][$permissionId][$aceId])) {
                throw new ACLException('Specified ACE is already registered');
            }
            if (!isset($this->ACEs[$roleId][$resourceId][$permissionId])) {
                $this->ACEs[$roleId][$resourceId][$permissionId] = array();
            }
            foreach ($this->ACEs[$roleId][$resourceId][$permissionId] as $existingACE) {
                if ($existingACE == $ace) {
                    continue 2;
                }
            }
            $this->ACEs[$roleId][$resourceId][$permissionId][$aceId] = $ace;
        }

        return $this;

    }

    public function removeACE(ACEInterface $ace)
    {
        $role = $ace->getRole();
        $resource = $ace->getResource();
        $permissions = $ace->getPermissions();
        $roleId = spl_object_hash($role);
        $resourceId = spl_object_hash($resource);
        $aceId = spl_object_hash($ace);
        foreach ($permissions as $permission) {
            $permissionId = spl_object_hash($permission);
            unset($this->ACEs[$roleId][$resourceId][$permissionId][$aceId]);

        }

        return $this;

    }

    public function hasACE(ACEInterface $ace)
    {
        $role = $ace->getRole();
        $resource = $ace->getResource();
        $permissions = $ace->getPermissions();
        $roleId = spl_object_hash($role);
        $resourceId = spl_object_hash($resource);
        $aceId = spl_object_hash($ace);

        foreach ($permissions as $permission) {
            $permissionId = spl_object_hash($permission);
            if (isset($this->ACEs[$roleId][$resourceId][$permissionId][$aceId])) {
                return true;
            }

        }

        return false;

    }

    public function isAllowed(RoleInterface $role, ResourceInterface $resource, PermissionInterface $permission, array $params = array())
    {
        $roleId = spl_object_hash($role);
        $resourceId = spl_object_hash($resource);
        $permissionId = spl_object_hash($permission);

        $allowed = $this->searchACEs($roleId, $resourceId, $permissionId, $params, 1);
        if (!isset($allowed)) {
            $allowed = $this->searchParentResourceACEs($roleId,$resourceId, $permissionId, $params, 1);
        }
        if (!isset($allowed)) {

            $allowed = $this->searchParentRoleACEs($roleId, $resourceId, $permissionId, $params, 1);

        }
        if (isset($allowed)) {
            return $allowed;
        }

        return false;

    }

    protected function searchParentRoleACEs($roleId, $resourceId, $permissionId, array $params = array(), $level = 0)
    {
        $allowedAny = null;
        if (isset($this->rolesParents[$roleId]) && isset($this->resources[$resourceId]) && isset($this->permissions[$permissionId])) {

            foreach ($this->rolesParents[$roleId] as $parentRoleId => $parentRole) {

                $allowed = $this->searchACES($parentRoleId, $resourceId, $permissionId, $params, $level + 1);
                if (!isset($allowed)) {
                    $allowed = $this->searchParentResourceACEs($parentRoleId, $resourceId, $permissionId, $params, $level +1);

                }
                if (!isset($allowed)) {
                    $allowed = $this->searchParentRoleACEs($parentRoleId, $resourceId, $permissionId, $params, $level +1);
                }
                if (isset($allowed)) {

                    if (!$allowed) {
                        return $allowed;
                    } else {
                        $allowedAny = true;
                    }

                }

            }

        }

        return $allowedAny;
    }

    protected function searchParentResourceACEs($roleId, $resourceId, $permissionId, array $params = array(), $level=0)
    {
        $allowedAny = null;
        if (isset($this->resourcesParents[$resourceId]) && isset($this->roles[$roleId]) && isset($this->permissions[$permissionId])) {

            foreach ($this->resourceParents[$resourceId] as $parentResourceId => $parentResource) {
                $allowed = $this>searchACEs($roleId, $parentResourceId, $permissionId, $params, $level + 1);
                if (!isset($allowed)) {
                    $allowed = $this->searchParentResourceACEs($roleId, $parentResourceId, $permissionId, $params, $level +1);
                }

                if (isset($allowed)) {
                    if (!$allowed) {
                        return $allowed;

                    } else {

                        $allowedAny = true;
                    }

                }

            }

        }

        return $allowedAny;
    }

    protected function searchACEs($roleId, $resourceId, $permissionId, array $params = array(), $level = 0)
    {
        if (!isset($this->ACEs[$roleId][$resourceId][$permissionId])) {
            return null;
        } else {
            $allowedAny = null;
            foreach ($this->ACEs[$roleId][$resourceId][$permissionId] as $ace) {
                $allowed = $ace->isAllowed($params);
                if (isset($allowed)) {
                    if (!$allowed) {
                        return false;
                    } else {
                        $allowedAny = true;
                    }

                }

            }

            return $allowedAny;
        }

    }

    private function stringify($object)
    {

        if (method_exists($object,'__toString')) {
            return (string) $object;
        } else {
            return get_class($object);
        }

    }

}
