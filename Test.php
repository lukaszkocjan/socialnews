<?php

use MyAcl\ACL;
use MyAcl\PermissionSimple;
use MyAcl\ResourceSimple;
use MyAcl\RoleSimple;
use MyAcl\ACEAllow;

function __autoload($class_name)
{
        echo 'Laduje klase'.$class_name . '<br>';
        require_once $class_name . '.php';
}

$acl = new ACL();

$view = PermissionSimple::factory('view');

$userRole = RoleSimple::factory('user');

$newsResource = ResourceSimple::factory('newsClass');

$acl->addPermission($view);
$acl->addRole($userRole);
$acl->addResource($newsResource);

$acl->addACE(new ACEAllow($userRole, $newsResource, $view));

var_dump($acl->isAllowed($userRole, $newsResource, $view));
